from django.test import TestCase, Client
from django.apps import apps
from event.apps import EventConfig
from .models import Event, Member
from .forms import CreateEvent, CreateMember

# Create your tests here.
class EventUnitTest(TestCase):
    # test app
    def test_apps(self):
        self.assertEqual(EventConfig.name, 'event')
        self.assertEqual(apps.get_app_config('event').name, 'event')
    
    # test URL Pertama /event/
    def test_story_6_event_url_is_exist(self):
        response = Client().get('/Event/')
        self.assertEqual(response.status_code, 200)
    
    def test_story_6_event_is_using_template(self):
        response = Client().get('/Event/')
        self.assertTemplateUsed(response, "Event.html")
    
    # test add data to database
    def test_model_can_create_new_event(self):
        new_event = Event.objects.create(name='ngantuk jam 2 udah')
        test_all_event = Event.objects.all().count()
        self.assertEqual(test_all_event, 1)
    
    def test_model_can_create_new_member(self):
        new_event = Event.objects.create(name='kenak ujan lagi')
        new_member = Member.objects.create(name='memberTest', event=new_event)
        test_all_member = Member.objects.all().count()
        self.assertEqual(test_all_member, 1)
    
    # test print database object name
    def test_str_model_event(self):
        event = Event.objects.create(name='uda jam 3')
        self.assertEqual(event.__str__(), 'uda jam 3')
    
    def test_str_model_member(self):
        event = Event.objects.create(name='sekarang jam 5 bos')
        member = Member.objects.create(name="memberTest", event=event)
        self.assertEqual(member.__str__(), 'memberTest')
    
    # test URL name kedua /addEvent/
    def test_story_6_url_add_event_is_exist(self):
        response = Client().get('/Event/addEvent/')
        self.assertEqual(response.status_code, 200)
    
    def test_story_6_add_event_is_using_template(self):
        response = Client().get('/Event/addEvent/')
        self.assertTemplateUsed(response, "addEvent.html")

    # test form event
    def test_valid_form_event(self):
        name = "sekalian subuh dah"
        response = Client().post('/Event/addEvent/', {'name': name})
        self.assertEqual(response.status_code, 302)
    
    # test URL name ketiga /addMember/
    def test_story_6_url_add_member_is_exist(self):
        event = Event.objects.create(name='ayok subuh')
        eventId = event.id
        response = Client().get('/Event/addMember/' + str(eventId))
        print(response)
        self.assertEqual(response.status_code, 200)
    
    def test_story_6_add_member_is_using_template(self):
        event = Event.objects.create(name='subuh pun lewat dan ini masi blum kelar')
        eventId = event.id
        response = Client().get('/Event/addMember/'+ str(eventId))
        print(response)
        self.assertTemplateUsed(response, "addMember.html")

    # test form member
    def test_valid_form_member(self):
        new_event = Event.objects.create(name='makin ngantuk')
        eventId = new_event.id
        name = "nameMember"
        response = Client().post('/Event/addMember/' + str(eventId), {'name': name, 'eventId' : eventId})
        print(response)
        self.assertEqual(response.status_code, 302)

     # test URL Pertama /event/
    def test_story_6_delete_url_is_redierect(self):
        new_event = Event.objects.create(name='fix sakit sih gara-gara ppw')
        new_member = Member.objects.create(name='member', event=new_event)
        memberId = new_member.id
        response = Client().get('/Event/deleteMember/' + str(memberId))
        self.assertEqual(response.status_code, 302)
    
    # test add data to database
    def test_model_can_delete_new_event(self):
        new_event = Event.objects.create(name='doain sembuh yeee... AMIN')
        new_member = Member.objects.create(name='member', event=new_event)
        test_all_member = Member.objects.all().count()
        new_member.delete()
        test_all_member_after_delete = Member.objects.all().count()
        self.assertNotEqual(test_all_member, test_all_member_after_delete)