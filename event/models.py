from django.db import models

# Create your models here.
class Event(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class Member(models.Model):
    name = models.CharField(max_length=50)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.name