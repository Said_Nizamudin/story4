from django.urls import path
from .views import event,addEvent,addMember,deleteMember

app_name = "event"

urlpatterns = [
    path('',event, name='Event'),
    path('addEvent/',addEvent, name='addEvent'),
    path('addMember/<str:id>',addMember, name='addMember'),
    path('deleteMember/<str:id>',deleteMember, name='deleteMember')

]
