from django.shortcuts import render, redirect
from .models import Jadwal
from .forms import SchedForm

# Create your views here.

def join(request):
    if request.method == 'POST':
        form = SchedForm(request.POST)
        if form.is_valid():
            jdwl = Jadwal()
            jdwl.matkul = form.cleaned_data['matkul']
            jdwl.dosen = form.cleaned_data['dosen']
            jdwl.sks = form.cleaned_data['sks']
            jdwl.deskripsi = form.cleaned_data['deskripsi']
            jdwl.semester = form.cleaned_data['semester']
            jdwl.kelas = form.cleaned_data['kelas']
            jdwl.save() 
        return redirect("/Jadwal")
    else:
        jdwl = Jadwal.objects.all()
        form = SchedForm()
        response = {'jadwal':jdwl, 'form': form}
        return render(request, 'Jadwal.html', response)

def delete(request, pk):
    # if request.method == 'GET':
    #     form = SchedForm(request.POST)
    #     jdwl = Jadwal()
    #     jdwl.objects.filter(pk = pk).delete()
    #     data = jdwl.objects.all()
    #     form = SchedForm()
    #     response = {
    #         "jadwal" : data,
    #         "form": form
    #     }
    #     return render(request, 'Jadwal.html', response)
    jdwl = Jadwal.objects.get(pk=pk)
    jdwl.delete()
    return redirect("/Jadwal")

def detail(request, pk):
    matkul = Jadwal.objects.get(pk=pk)
    response1= {
        "matkul" : matkul.matkul,
        "dosen" : matkul.dosen,
        "sks" : matkul.sks,
        "deskripsi" : matkul.deskripsi,
        "semester" : matkul.semester,
        "kelas" : matkul.kelas,
    }
    return render(request, 'Detail.html', response1)    