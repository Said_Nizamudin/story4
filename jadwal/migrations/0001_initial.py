# Generated by Django 3.1.2 on 2020-10-17 06:06

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Jadwal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('matkul', models.CharField(max_length=50)),
                ('dosen', models.CharField(max_length=50)),
                ('sks', models.IntegerField()),
                ('deskripsi', models.CharField(help_text='jangan kepanjangan ya :)', max_length=100)),
                ('semester', models.CharField(max_length=50)),
                ('kelas', models.CharField(help_text='contoh: Gasal 2019/2020', max_length=50)),
            ],
        ),
    ]
