from django import forms

class SchedForm(forms.Form):
    matkul = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Mata Kuliah Kamu',
        'type' : 'text',
        'required' : True,
    }))
    dosen = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Nama Dosen Kamu',
        'type' : 'text',
        'required' : True,
    }))
    sks = forms.IntegerField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Jumlah SKS Kamu',
        'type' : 'text',
        'required' : True,
    }))
    deskripsi = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Deskripsi Mata Kuliah Kamu',
        'type' : 'text',
        'required' : True,
    }))
    semester = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Semester Kamu Sekarang',
        'type' : 'text',
        'required' : True,
    }))
    kelas = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Di Kelas Mana Matkulnya',
        'type' : 'text',
        'required' : True,
    }))