from django.db import models

# Create your models here.
class Jadwal(models.Model):
    matkul = models.CharField(blank=False, max_length= 50)
    dosen = models.CharField(blank=False, max_length= 50)
    sks = models.IntegerField(blank=False)
    deskripsi = models.CharField(blank=False, max_length= 100, help_text ='jangan kepanjangan ya :)')
    semester = models.CharField(blank=False, max_length= 50)
    kelas = models.CharField(blank=False, max_length= 50, help_text ='contoh: Gasal 2019/2020')