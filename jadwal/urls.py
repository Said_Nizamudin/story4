from django.urls import path
from .views import join, delete, detail

app_name = 'jadwal'

urlpatterns = [
    path('', join, name='Jadwal'),
    path('delete/<int:pk>', delete, name = 'delete'),
    path('detail/<int:pk>', detail, name='detail'),
]