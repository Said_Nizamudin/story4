from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, 'Home.html')

def phortopholio(request):
    return render(request, 'Phortopholio.html')

def story1(request):
    return render(request, 'Story1.html')