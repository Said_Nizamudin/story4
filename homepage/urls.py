from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='Home'),
    path('phortopholio/', views.phortopholio, name='Phortopholio'),
    path('story1/', views.story1, name='Story1'),
]